package tk.labyrinth.trademarket;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.trademarket.technology.Logo;
import tk.labyrinth.trademarket.technology.Technology;
import tk.labyrinth.trademarket.technology.TechnologyService;

import javax.annotation.Nullable;

@RequiredArgsConstructor
@Route("")
public class IndexPage extends VerticalLayout {

	private final TextField filterField = new TextField(null, "Filter");

	private final Grid<Technology> grid = new Grid<>();

	private final TechnologyService technologyService;

	private final Button themeButton = new Button();

	private Parameters parameters = Parameters.builder()
			.dark(false)
			.filter(null)
			.build();

	@PostConstruct
	private void postConstruct() {
		{
			setHeightFull();
		}
		{
			{
				HorizontalLayout header = new HorizontalLayout();
				{
					header.setAlignItems(Alignment.CENTER);
					header.setHeight("3em");
				}
				{
					header.add(new RouterLink("TODO:LOGO", IndexPage.class));
					{
						filterField.setClearButtonVisible(true);
						filterField.addValueChangeListener(event -> {
							parameters = parameters.withFilter(event.getValue());
							refresh();
						});
						filterField.setValueChangeMode(ValueChangeMode.EAGER);
						//
						header.addAndExpand(filterField);
					}
					{
						Button requestButton = new Button("Request");
						//
						requestButton.setEnabled(false);
						//
						header.add(requestButton);
					}
					{
						themeButton.addClickListener(event -> {
							parameters = parameters.withDark(!parameters.dark());
							refresh();
						});
						//
						header.add(themeButton);
					}
					header.add(new RouterLink("About", AboutPage.class));
				}
				add(header);
			}
			{
				{
					grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
				}
				{
					grid
							.addColumn(Technology::getName)
							.setHeader("Name")
							.setResizable(true);
					grid
							.addComponentColumn(item -> renderLogo(item.getIconLogo(), item.getName()))
							.setHeader("Icon")
							.setResizable(true);
					grid
							.addComponentColumn(item -> renderLogo(item.getHorizontalLogo(), item.getName()))
							.setFlexGrow(2)
							.setHeader("Horizontal")
							.setResizable(true);
					grid
							.addComponentColumn(item -> renderLogo(item.getStackedLogo(), item.getName()))
							.setHeader("Stacked")
							.setResizable(true);
					grid
							.addComponentColumn(item -> {
								HorizontalLayout layout = new HorizontalLayout();
								{
									layout.getStyle().set("flex-wrap", "wrap");
									layout.getThemeList().remove("padding");
								}
								{
									item.getTags().forEach(tag -> {
										Span span = new Span(tag);
										//
										span.getElement().getThemeList().add("badge pill");
										//
										span.addClickListener(event -> {
											parameters = parameters.withFilter(tag);
											refresh();
										});
										//
										layout.add(span);
									});
								}
								return layout;
							})
							.setFlexGrow(2)
							.setHeader("Tags")
							.setResizable(true);
					{
						Button contributeNewButton = new Button("Contribute New");
						{
							contributeNewButton.setEnabled(false);
						}
						//
						grid
								.addComponentColumn(item -> {
									Button contributeButton = new Button("Contribute");
									//
									contributeButton.setEnabled(false);
									//
									return contributeButton;
								})
								.setAutoWidth(true)
								.setFlexGrow(0)
								.setHeader(contributeNewButton);
					}
				}
				add(grid);
			}
		}
		{
			refresh();
		}
	}

	private void refresh() {
		Parameters parameters = this.parameters;
		String filter = parameters.filter();
		String filterToUse = filter != null ? filter.toLowerCase() : null;
		//
		{
			themeButton.setText(parameters.dark() ? "Light" : "Dark");
			getThemeList().set("dark", parameters.dark());
		}
		{
			filterField.setValue(filter != null ? filter : "");
			grid.setItems(technologyService.getTechnologies()
					.filter(technology -> filterToUse == null || matches(technology, filterToUse))
					.asJava());
		}
	}

	private static boolean matches(Technology technology, String filter) {
		return technology.getName().toLowerCase().contains(filter)
				|| technology.getTags().exists(tag -> tag.toLowerCase().contains(filter));
	}

	public static Component renderLogo(@Nullable Logo logo, String technologyCanonicalName) {
		Component result;
		{
			if (logo != null) {
				Image image = new Image(logo.getImageUrl(), null);
				//
				image.setMaxHeight("4em");
				image.setMaxWidth("100%");
				//
				result = image;
			} else {
				String searchQuery = "%s logo".formatted(technologyCanonicalName);
				//
				Anchor anchor = new Anchor(
						"https://duckduckgo.com/?ia=images&iax=images&q=%s".formatted(searchQuery),
						"Search",
						AnchorTarget.BLANK);
				//
				result = anchor;
			}
		}
		return result;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		Boolean dark;

		@Nullable
		String filter;
	}
}
