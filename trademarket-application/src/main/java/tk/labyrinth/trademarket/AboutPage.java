package tk.labyrinth.trademarket;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import jakarta.annotation.PostConstruct;

@Route("about")
public class AboutPage extends VerticalLayout {

	@PostConstruct
	private void postConstruct() {
		{
			setHeightFull();
		}
		{
			{
				HorizontalLayout header = new HorizontalLayout();
				{
					header.setAlignItems(Alignment.CENTER);
					header.setHeight("3em");
				}
				{
					header.add(new RouterLink("TODO:LOGO", IndexPage.class));
				}
				add(header);
			}
			{
				add(new Span("This service is designed to aggregate high-quality " +
						"logos of various technologies for easy search and copying."
				));
				add(new Span("The idea came up when author was gathering considerable " +
						"number of logos for a tech-stack slide of a presale presentation."));
				add(new Span("Note that the service aggregates links to other resources " +
						"and does not store them on its own, so any legal issues of using them " +
						"are still to be taken into account for your specific purposes."));
			}
			{
				add(new H3("Contact Us:"));
				//
				{
					HorizontalLayout telegramLayout = new HorizontalLayout();
					{
						telegramLayout.add("Telegram:");
						telegramLayout.add(new Anchor("https://t.me/commi_union", "commi-union"));
					}
					add(telegramLayout);
				}
			}
			{
				add(new H3("Powered By:"));
				//
				add(new Span("Language: Java 17"));
				add(new Span("Server Framework: Quarkus"));
				add(new Span("Frontend Framework: Vaadin"));
				add(new Span("CI/CD: Gitlab"));
				add(new Span("Infrastructure: Render"));
			}
		}
	}
}
