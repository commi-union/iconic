package tk.labyrinth.trademarket.technology;

import io.vavr.collection.List;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@ApplicationScoped
public class TechnologyService {

	private final ConcurrentMap<UUID, Technology> technologies = new ConcurrentHashMap<>();

	{
		List
				.of(
						Technology.builder()
								.horizontalLogo(Logo.builder()
										.contextUrl("https://www.mongodb.com/brand-resources")
										.imageUrl("https://webimages.mongodb.com/_com_assets/cms/kuyjf3vea2hg34taa-horizontal_default_slate_blue.svg")
										.build())
								.name("MongoDB")
								.tags(List.of("NoSQL", "OpenSource"))
								.build(),
						Technology.builder()
								.iconLogo(Logo.builder()
										.contextUrl("https://wiki.postgresql.org/wiki/Logo")
										.imageUrl("https://wiki.postgresql.org/images/a/a4/PostgreSQL_logo.3colors.svg")
										.build())
								.name("PostgreSQL")
								.tags(List.of("SQL", "OpenSource"))
								.build(),
						Technology.builder()
								.horizontalLogo(Logo.builder()
										.contextUrl("https://github.com/cncf/artwork/tree/master/projects/prometheus")
										.imageUrl("https://raw.githubusercontent.com/cncf/artwork/7e1e367a5b30b3849953ab5a0133052b31691d5d/projects/prometheus/horizontal/color/prometheus-horizontal-color.svg")
										.build())
								.iconLogo(Logo.builder()
										.contextUrl("https://github.com/cncf/artwork/tree/master/projects/prometheus")
										.imageUrl("https://raw.githubusercontent.com/cncf/artwork/7e1e367a5b30b3849953ab5a0133052b31691d5d/projects/prometheus/icon/color/prometheus-icon-color.svg")
										.build())
								.name("Prometheus")
								.stackedLogo(Logo.builder()
										.contextUrl("https://github.com/cncf/artwork/tree/master/projects/prometheus")
										.imageUrl("https://raw.githubusercontent.com/cncf/artwork/7e1e367a5b30b3849953ab5a0133052b31691d5d/projects/prometheus/stacked/color/prometheus-stacked-color.svg")
										.build())
								.tags(List.of("Metrics", "OpenSource"))
								.build(),
						Technology.builder()
								.horizontalLogo(Logo.builder()
										.contextUrl("https://temporal.io/")
										.imageUrl("https://temporal.io/images/logos/logo-temporal-with-copy.svg")
										.build())
								.name("Temporal")
								.tags(List.of("OpenSource", "Workflow"))
								.build(),
						Technology.builder()
								.horizontalLogo(Logo.builder()
										.contextUrl("https://github.com/cncf/artwork/tree/master/projects/kubernetes")
										.imageUrl("https://raw.githubusercontent.com/cncf/artwork/7e1e367a5b30b3849953ab5a0133052b31691d5d/projects/kubernetes/horizontal/white-text/kubernetes-horizontal-white-text.svg")
										.build())
								.iconLogo(Logo.builder()
										.contextUrl("https://github.com/cncf/artwork/tree/master/projects/kubernetes")
										.imageUrl("https://raw.githubusercontent.com/cncf/artwork/7e1e367a5b30b3849953ab5a0133052b31691d5d/projects/kubernetes/icon/color/kubernetes-icon-color.svg")
										.build())
								.name("Kubernetes")
								.stackedLogo(Logo.builder()
										.contextUrl("https://github.com/cncf/artwork/tree/master/projects/kubernetes")
										.imageUrl("https://raw.githubusercontent.com/cncf/artwork/7e1e367a5b30b3849953ab5a0133052b31691d5d/projects/kubernetes/stacked/white-text/kubernetes-stacked-white-text.svg")
										.build())
								.tags(List.of("ContainerOrchestrator", "Infrastructure", "OpenSource"))
								.build(),
						Technology.builder()
								.horizontalLogo(Logo.builder()
										.contextUrl("https://vaadin.com/trademark")
										.imageUrl("https://cdn2.hubspot.net/hubfs/1840687/Pages/trademark/vaadin-logo-full.svg")
										.build())
								.iconLogo(Logo.builder()
										.contextUrl("https://vaadin.com/trademark")
										.imageUrl("https://cdn2.hubspot.net/hubfs/1840687/Pages/trademark/vaadin-logo-1.svg")
										.build())
								.name("Vaadin")
								.tags(List.of("Frontend", "Java", "OpenSource"))
								.build())
				.map(technology -> technology.withUid(UUID.randomUUID()))
				.forEach(technology -> technologies.put(technology.getUid(), technology));
	}

	public List<Technology> getTechnologies() {
		return List.ofAll(technologies.values());
	}
}
