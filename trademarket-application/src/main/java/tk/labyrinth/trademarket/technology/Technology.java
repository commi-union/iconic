package tk.labyrinth.trademarket.technology;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class Technology {

	Logo horizontalLogo;

	Logo iconLogo;

	String name;

	Logo stackedLogo;

	List<String> tags;

	UUID uid;
}
