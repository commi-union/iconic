package tk.labyrinth.trademarket.technology;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class Logo {

	String contextUrl;

	String imageUrl;
}
